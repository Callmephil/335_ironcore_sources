/*
Original Script By : Callmephil
Current Supported Version : TrinityCore 3.3.5 (60)
Original Idea : http://wowchallenges.com/index.php?Show=IronManRules
Script Progression : 20 %
*/

/*
SQL :
ALTER TABLE `characters`
ADD COLUMN `IronMan` TINYINT(2) UNSIGNED NOT NULL DEFAULT '0';
*/
#include "Cha_Leveling_NPC.h"
#include "Cha_Leveling.h"



class Leveling_NPC : public CreatureScript
{
public:
	Leveling_NPC() : CreatureScript("Leveling_NPC"){}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		// Text Information About Options
		// Send to SubMenu that then confirm request
		player->ADD_GOSSIP_ITEM(1, M_ICO_NORMAL M_TXT_QUEUE_TO_NORMAL_MODE, GOSSIP_SENDER_MAIN, INFO_NORMAL_MODE);
		player->ADD_GOSSIP_ITEM(1, M_ICO_MEDIUM M_TXT_QUEUE_TO_MEDIUM_MODE, GOSSIP_SENDER_MAIN, INFO_MEDIUM_MODE);
		player->ADD_GOSSIP_ITEM(1, M_ICO_HARD M_TXT_QUEUE_TO_HARD_MODE, GOSSIP_SENDER_MAIN, INFO_HARD_MODE);
		player->ADD_GOSSIP_ITEM(1, M_ICO_TORMENT DEAD_ICON M_TXT_QUEUE_TO_TORMENT_MODE, GOSSIP_SENDER_MAIN, INFO_TORMENT_MODE);

		player->PlayerTalkClass->SendGossipMenu(CHA_NPC_TXT_MAIN, creature->GetGUID());
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 /* Sender */, uint32 Action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (Action)
		{
		case MAIN_MENU:
			OnGossipHello(player, creature);
			break;

			// Open DISPLAY TEXT TO EXPLAIN CHALLENGE & REWARD (DB SIDE)
		case INFO_NORMAL_MODE:
			player->ADD_GOSSIP_ITEM(0, M_ICO_QUEUE M_TEXT_CONFIRM " - Normal Difficulty", GOSSIP_SENDER_MAIN, Q_NORMAL_MODE);
			player->ADD_GOSSIP_ITEM(0, M_TXT_BACK, GOSSIP_SENDER_MAIN, MAIN_MENU);
			player->SEND_GOSSIP_MENU(CHA_NPC_TXT_NORMAL, creature->GetGUID());
			break;
		case INFO_MEDIUM_MODE:
			player->ADD_GOSSIP_ITEM(0, M_ICO_QUEUE M_TEXT_CONFIRM " - Medium Difficulty", GOSSIP_SENDER_MAIN, Q_MEDIUM_MODE);
			player->ADD_GOSSIP_ITEM(0, M_TXT_BACK, GOSSIP_SENDER_MAIN, MAIN_MENU);
			player->SEND_GOSSIP_MENU(CHA_NPC_TXT_MEDIUM, creature->GetGUID());
			break;
		case INFO_HARD_MODE:
			player->ADD_GOSSIP_ITEM(0, M_ICO_QUEUE M_TEXT_CONFIRM " - Hard Difficulty", GOSSIP_SENDER_MAIN, Q_HARD_MODE);
			player->ADD_GOSSIP_ITEM(0, M_TXT_BACK, GOSSIP_SENDER_MAIN, MAIN_MENU);
			player->SEND_GOSSIP_MENU(CHA_NPC_TXT_HARD, creature->GetGUID());
			break;
		case INFO_TORMENT_MODE:
			player->ADD_GOSSIP_ITEM(0, M_ICO_QUEUE M_TEXT_CONFIRM " - Torment Difficulty", GOSSIP_SENDER_MAIN, Q_TORMENT_MODE);
			player->ADD_GOSSIP_ITEM(0, M_TXT_BACK, GOSSIP_SENDER_MAIN, MAIN_MENU);
			player->SEND_GOSSIP_MENU(CHA_NPC_TXT_TORMENT, creature->GetGUID());
			break;

			// WHEN CONFIRM IS DONE THEN CHANGES RATES FOR CHARACTERS
		case Q_NORMAL_MODE:
			//sExtensionChallenges->Queue_Normal_Mode(player);
			break;

		case Q_MEDIUM_MODE:
			//sExtensionChallenges->Queue_Medium_Mode(player);
			break;

		case Q_HARD_MODE:
			//sExtensionChallenges->Queue_Hard_Mode(player);
			break;

		case Q_TORMENT_MODE:
			//sExtensionChallenges->Queue_Torment_Mode(player);
			break;
		}

		return true;
	}
};

void AddSC_Leveling_NPC()
{
	new Leveling_NPC();
}
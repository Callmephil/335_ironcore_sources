/*
Original Script By : Callmephil
Current Supported Version : SunWellCore/IronCore 3.3.5 (2009)
Original Idea : PaTiiCoS | Mathias (Skype)
Script Progression : 70 %
Todo : Recode Queries System
*/

enum Guild_Menu
{
	MENU_NEVERMIND,
	MENU_GUILDINFO,
	MENU_BONUSINFO,
	MENU_GUILDREGI,
	MENU_GUILDUNLIST,
	MENU_APPLICANT,
	ACTION_GUILDQUEUE,

	/* Donations Menu */
	MENU_D_LEVEL,
	MENU_D_RENAME,
};

#define D_TOKEN 500000
#define GOLD_REQ 2000 * GOLD

#include "ScriptPCH.h"
#include "Config.h"
#include "Chat.h"
#include "Language.h"
#include "Guild.h"
#include "GuildMgr.h"

void LoadGuildFinderInfo(Player* player, Creature* creature/*, uint32 page*/)
{
	uint32 oldMSTime = getMSTime();

	QueryResult result = CharacterDatabase.Query("SELECT `guildId`, `name`, `listed`, `faction` FROM `guild` ORDER BY `guildId` ASC LIMIT 30");
	if (!result)
		return;

	do
	{
		Field * fields = result->Fetch();

		std::ostringstream result_string;

		uint32 guildId = fields[0].GetUInt32();
		std::string guildName = fields[1].GetString();
		// uint8 guildLevel = fields[2].GetUInt8();
		uint32 Listed = fields[2].GetUInt32();
		uint32 factionID = fields[3].GetUInt32();

	/*	uint32 startPos = (page - 1) * 10;
		uint32 currentPos = 0;
		uint32 i, test;
		test = result + 1;*/
		// guildLevel = 0;

		//result_string << guildId << ". Name : [" << guildName.c_str() << "]";", Level : [" << guildLevel << "] | r";

		result_string << guildId << ". Name : [" << guildName.c_str() << "]";

		/*for (i = 0; i < test; ++i)
		{
			if (currentPos >= startPos)
			{*/
				if (Listed == 1 && (player->GetTeamId() == factionID || factionID == 0)) // Only Show Guild from the same faction as the player or Community Guilds (Crossfaction)
					player->ADD_GOSSIP_ITEM(GOSSIP_ICON_BATTLE, result_string.str().c_str(), GOSSIP_SENDER_MAIN, ACTION_GUILDQUEUE);
		//	}
		
	/*	currentPos++;
		// Stop looping once we are 10 positions ahead
		if (currentPos == startPos + 10)
			return;
		}
		
		// Make sure there is actually another page
		if (currentPos == startPos + 10 && Listed == 1)
			player->ADD_GOSSIP_ITEM(0, "Next Page", 100 + page + 1, ACTION_GUILDQUEUE);  // Have to offset by 100 to ensure it gets pushed to the default case
		player->ADD_GOSSIP_ITEM(0, "Back", 100 + page - 1, ACTION_GUILDQUEUE);
		*/
	} while (result->NextRow());
	player->SEND_GOSSIP_MENU(68, creature->GetGUID());
	return;
}

void LoadGuildLevelInfo(Player* player)
{
	uint16 GuildLevel = player->GetGuild()->GetLevel();
	std::stringstream GuildInfo1;
	std::stringstream GuildInfo2;

	if (player->GetGuild())
	{
		if (player->GetGuild()->GetLevel() >= 8)
		{
			GuildInfo1 << "[Guild Level] : " << GuildLevel;
			GuildInfo2 << "[Guild Experience] : " << "Max/Max" << "|r";
		}
		else
		{
			GuildInfo1 << "[Guild Level] : " << GuildLevel;
			GuildInfo2 << "[Guild Experience] : " << player->GetGuild()->GetCurrentXP() << "/" << player->GetGuild()->GetXpForNextLevel() << "|r";
		}

		player->ADD_GOSSIP_ITEM(0, GuildInfo1.str(), MENU_GUILDINFO, MENU_GUILDINFO);
		player->ADD_GOSSIP_ITEM(0, GuildInfo2.str(), MENU_GUILDINFO, MENU_GUILDINFO);
	}

}

void LoadGuildPerkCommand(Player* player)
{
	Guild* guild = player->GetSession()->GetPlayer()->GetGuild();

	if (player->GetGuild())
	{
		if (guild->GetLevel() > 0)
		{
			if (guild->HasLevelForBonus(GUILD_BONUS_GOLD_1) && !guild->HasLevelForBonus(GUILD_BONUS_GOLD_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Gold bonus [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_XP_1) && !guild->HasLevelForBonus(GUILD_BONUS_XP_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Experience [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_SCHNELLER_GEIST))
				ChatHandler(player->GetSession()).PSendSysMessage("Faster Ghost");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_1) && !guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Cheaper Repairs [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_GOLD_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Gold bonus [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REITTEMPO_1) && !guild->HasLevelForBonus(GUILD_BONUS_RUF_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Mount Speed [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_RUF_1) && !guild->HasLevelForBonus(GUILD_BONUS_RUF_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Reputation [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_XP_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Experience [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Cheaper Repairs [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REITTEMPO_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Mount Speed [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_REPERATUR_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Reputation [Rank 2]");
			if (guild->HasLevelForBonus(GUILD_BONUS_EHRE_1) && !guild->HasLevelForBonus(GUILD_BONUS_EHRE_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Honor [Rank 1]");
			if (guild->HasLevelForBonus(GUILD_BONUS_EHRE_2))
				ChatHandler(player->GetSession()).PSendSysMessage("Bonus Honor [Rank 2]");
		}
		else
			ChatHandler(player->GetSession()).PSendSysMessage("None");
	}
}

class GuildApplicant_NPC : public CreatureScript
{
public:
	GuildApplicant_NPC() : CreatureScript("GuildApplicant_NPC"){}

	bool OnGossipHello(Player* player, Creature* creature)
	{
		Guild * pGuild = player->GetGuild();
		player->PlayerTalkClass->ClearMenus();

		// List Participant
		if (player->GetGuild())
		{
			player->ADD_GOSSIP_ITEM(5, "===== Guild Informations Tools =====", MENU_GUILDINFO, MENU_GUILDINFO);
			LoadGuildLevelInfo(player);
			player->ADD_GOSSIP_ITEM(0, "Active Bonuses", MENU_BONUSINFO, MENU_BONUSINFO);

			if (pGuild && (pGuild->GetLeaderGUID() == player->GetGUIDLow()))
			{
				player->ADD_GOSSIP_ITEM(5, "===== Guild Masters Tools =====", MENU_GUILDINFO, MENU_GUILDINFO);
				// This is needed since ADD_GOSSIP_ITEM does not handle extra thingy don t know why
				// if (player->GetGuild()->IsListed(0))
				player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, 10, "List My Guild (2000 Gold)", MENU_GUILDREGI, MENU_GUILDREGI, "Are You Sure ?", GOLD_REQ);
				// else 
				player->PlayerTalkClass->GetGossipMenu().AddMenuItem(-1, 10, "Unlist My Guild", MENU_GUILDUNLIST, MENU_GUILDUNLIST, "Are You Sure ?", 0);
				player->ADD_GOSSIP_ITEM(5, "===== Guild Donation Tools =====", MENU_GUILDINFO, MENU_GUILDINFO);
				player->ADD_GOSSIP_ITEM(1, "Buy : Guild Level [+1] (x5 D-Token)", MENU_D_LEVEL, MENU_D_LEVEL);
				player->ADD_GOSSIP_ITEM_EXTENDED(1, "Buy : Guild Rename (x7 D-Token)", MENU_D_RENAME, MENU_D_RENAME, "", 0, true);
			}
		}
		else if (!player->GetGuild()) {
			player->ADD_GOSSIP_ITEM(0, "Join & View Guild List", MENU_APPLICANT, MENU_APPLICANT);
			player->ADD_GOSSIP_ITEM(0, "Nevermind", MENU_NEVERMIND, MENU_NEVERMIND);
		}

		player->PlayerTalkClass->SendGossipMenu(1, creature->GetGUID());
		return true;
	}

	bool OnGossipSelectCode(Player* player, Creature* creature, uint32 Sender, uint32 /*Action*/, const char* GuildName)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (Sender)
		{
		case MENU_D_RENAME:
			// @todo Check For Numeric Value in GuildName
			if (player->GetGuild() && player->HasItemCount(D_TOKEN, 7, true))
			{
				CharacterDatabase.PExecute("UPDATE `guild` Set `name` = '%s' Where `guildid` = %u", GuildName, player->GetGuildId());
				player->DestroyItemCount(D_TOKEN, 7, true);
				player->GetSession()->SendNotification("Guild name got successfully changed, server will update it soon.");
			}
			else
				player->GetSession()->SendNotification("You don't have enough D-Token for that.");

			OnGossipHello(player, creature);
			break;
		}
		return true;
	}

	bool OnGossipSelect(Player* player, Creature* creature, uint32 Sender, uint32 Action)
	{
		player->PlayerTalkClass->ClearMenus();

		switch (Sender)
		{
		case MENU_NEVERMIND:
			player->CLOSE_GOSSIP_MENU();
			break;

		case MENU_BONUSINFO:
			LoadGuildPerkCommand(player);
			player->CLOSE_GOSSIP_MENU();
			break;


		case MENU_GUILDINFO:
			OnGossipHello(player, creature);
			break;

		case MENU_GUILDREGI:
			if (player->GetGuild()) // In Case Someone Leave Guild Just Before
			{
				player->GetGuild()->ListGuild(1, player->GetTeamId());
				player->ModifyMoney(-GOLD_REQ, true);
				player->GetSession()->SendNotification("Guild Listed With Succes");
			}
			OnGossipHello(player, creature);
			break;

		case MENU_GUILDUNLIST:
			if (player->GetGuild()) // In Case Someone Leave Guild Just Before
				player->GetGuild()->ListGuild(0, player->GetTeamId());


			player->GetSession()->SendNotification("Guild Unlisted With Succes");
			OnGossipHello(player, creature);
			break;

		case MENU_D_LEVEL:
			if (player->GetGuild() && player->HasItemCount(D_TOKEN, 5, true)) // In Case Someone Leave Guild just before
			{
				if (player->GetGuild()->GetLevel() >= GUILD_MAX_LEVEL)
					player->GetSession()->SendNotification("Your Guild at max level");
				else
				{
					uint8 NewLevel = player->GetGuild()->GetLevel() + 1;
					player->GetGuild()->SetLevel(NewLevel, false);
					player->DestroyItemCount(D_TOKEN, 5, true);
				}
			}
			else
				player->GetSession()->SendNotification("You don't have enough D-Token for that.");

			player->CLOSE_GOSSIP_MENU();
			OnGossipHello(player, creature);
			break;

		case MENU_APPLICANT:
			if (!player->GetGuild()) // in case someone join a guild after talking to the npc do not allow him this.
				LoadGuildFinderInfo(player, creature/*, Action*/);
			else
				OnGossipHello(player, creature);
			break;
		}

		if (Action == ACTION_GUILDQUEUE && !player->GetGuild())
		{
			Guild* guild = sGuildMgr->GetGuildById(Action);
			guild->AddMember(player->GetGUIDLow());
			OnGossipHello(player, creature);
		}

		return true;
	}
};

void AddSC_GuildInfo_NPC()
{
	new GuildApplicant_NPC();
}